import { Component, OnInit } from '@angular/core';
import { VehicleBrandModel } from 'src/app/models/vehicle-brand-model';
import { VehicleBrandService } from 'src/app/services/vehicle-brand.service';
import { AlertService } from 'src/app/shared/services/alert/alert.service';
import { DataService } from 'src/app/services/data.service';
import { AlertActionModel } from 'src/app/shared/models/alert-action-model';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-new-vehicle-brand',
  templateUrl: './new-vehicle-brand.component.html',
  styleUrls: ['./new-vehicle-brand.component.scss']
})
export class NewVehicleBrandComponent implements OnInit {
  vehicleBrandModel: VehicleBrandModel = {};
  brandID: string;

  constructor(
    private vehicleBrandService: VehicleBrandService,
    private alertService: AlertService,
    private dataService: DataService,
    private location: Location,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.dataService.setTitle('New Vehicle Brand');
    this.brandID = this.route.snapshot.paramMap.get('brandID');
    if (this.brandID != null) {
      this.get(this.brandID);
    }
  }

  /**
   * request to show vehicle brand record with particular Id
   * @param id particular vehicle brand Id
   */
  get(id: string) {
    this.vehicleBrandService.get(id).subscribe(res => {
      this.vehicleBrandModel = res;
    });
  }

  /**
   * function for API Post request this will invoke Create()
   */
  saveForm() {
    this.vehicleBrandService.createOrUpdate(this.vehicleBrandModel).subscribe(res => {
      if (res && res._id) {
        this.alertService.success(
          `Vehicle Brand ${this.vehicleBrandModel._id ? 'Updated' : 'Added'} successfully`, 'BrandAddorUpdate'
        );
        const alertListener = this.alertService.getAction('BrandAddorUpdate').subscribe((alertActionModel: AlertActionModel) => {
          if (alertActionModel.actionId === 1 && alertActionModel.functionName === 'BrandAddorUpdate') {
            alertListener.unsubscribe();
            this.actionBack();
          }
        });
      }
    });
  }

  /**
   * function to locate back
   */
  public actionBack() {
    this.location.back();
  }

}
