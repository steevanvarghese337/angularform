import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { OrderModel } from '../models/order';
import { CheckoutModel } from'../models/checkout'
import {
  HandleError,
  HttpErrorHandler,
} from 'src/app/network/http-error-handler.service';
import { ApiResponseModel } from '../models/api-response-model';

@Injectable({
  providedIn: 'root',
})
export class OrderService {
  private handleError: HandleError;
  private url: string;
  private url2:string;
  constructor(private http: HttpClient, httpErrorHandler: HttpErrorHandler) {
    this.url =
      environment.api.host +
      environment.api.port +
      environment.api.api +
      environment.api.v +
      environment.services.order;
    this.handleError = httpErrorHandler.createHandleError(
      'OrderService'
    );
    this.url2 =
    environment.api.host +
    environment.api.port +
    environment.api.api +
    environment.api.v +
    environment.services.checkout;
  this.handleError = httpErrorHandler.createHandleError(
    'OrderService'
  );
  }

  public createOrUpdate(orderModel: OrderModel) {
    if (orderModel._id) {
      return this.update(orderModel);
    } else {
      return this.create(orderModel);
    }
  }
  /**
   * submit a post API request to create new review record
   * @param OrderModel new review data
   */
  create(orderModel: OrderModel): Observable<OrderModel> {
    const inputData = 
  orderModel;
    return this.http
      .post<ApiResponseModel<OrderModel>>(this.url, inputData)
      .pipe(
        map((res) => res.data),
        catchError(this.handleError('create', null))
      );
  }
  /**
   * Submit a update API request to update a particular vehicle model record
   * @param vehicleModel particular vehicle model id
   */
  update(Order: OrderModel): Observable<OrderModel> {
    const inputData = {
      order: Order,
    };
    const url = `${this.url}/${Order._id}`;
    return this.http.put<ApiResponseModel<OrderModel>>(url, inputData).pipe(
      map((res) => res.data),
      catchError(this.handleError('update', null))
    );
  }
  createCheckout(checkoutModel: CheckoutModel): Observable<CheckoutModel> {
    const inputData = 
    checkoutModel;
    return this.http
      .post<ApiResponseModel<CheckoutModel>>(this.url2, inputData)
      .pipe(
        map((res) => res.data),
        catchError(this.handleError('create', null))
      );
  }
}