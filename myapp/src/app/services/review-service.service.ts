import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { ReviewModel } from '../models/review-model';
import {
  HandleError,
  HttpErrorHandler,
} from 'src/app/network/http-error-handler.service';
import { ApiResponseModel } from '../models/api-response-model';

@Injectable({
  providedIn: 'root',
})
export class ReviewServiceService {
  private handleError: HandleError;
  private url: string;
  constructor(private http: HttpClient, httpErrorHandler: HttpErrorHandler) {
    this.url =
      environment.api.host +
      environment.api.port +
      environment.api.api +
      environment.api.v +
      environment.services.review;
    this.handleError = httpErrorHandler.createHandleError(
      'ReviewServiceService'
    );
  }

  /**
   * function to create or update vehicle model record
   * @param vehicleModel select vehicle model record
   */

  public createOrUpdate(reviewModel: ReviewModel) {
    if (reviewModel._id) {
      return this.update(reviewModel);
    } else {
      return this.create(reviewModel);
    }
  }
  /**
   * submit a post API request to create new review record
   * @param reviewModel new review data
   */
  create(reviewModel: ReviewModel): Observable<ReviewModel> {
    const inputData = {
      review: reviewModel,
    };
    return this.http
      .post<ApiResponseModel<ReviewModel>>(this.url, inputData)
      .pipe(
        map((res) => res.data),
        catchError(this.handleError('create', null))
      );
  }
  /**
   * Submit a update API request to update a particular vehicle model record
   * @param vehicleModel particular vehicle model id
   */
  update(reviewModel: ReviewModel): Observable<ReviewModel> {
    const inputData = {
      review: reviewModel,
    };
    const url = `${this.url}/${reviewModel._id}`;
    return this.http.put<ApiResponseModel<ReviewModel>>(url, inputData).pipe(
      map((res) => res.data),
      catchError(this.handleError('update', null))
    );
  }
}
