import { ChangeDetectorRef } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ExternalLibraryService } from 'src/app/util';

declare let Razorpay: any;

@Component({
  selector: 'app-pay',
  templateUrl: './pay.component.html',
  styleUrls: ['./pay.component.scss']
})
export class PayComponent implements OnInit {

  constructor(private razorpayService: ExternalLibraryService, private cd:  ChangeDetectorRef) { }
  name = 'Angular';
  response;
  razorpayResponse;
  showModal = false;

  ngOnInit(): void { this.razorpayService
    .lazyLoadLibrary('https://checkout.razorpay.com/v1/checkout.js')
    .subscribe();
  }
  RAZORPAY_OPTIONS = {
    "key": "rzp_test_7HpekRp5jhnTOO",
    "amount": "",
    "name": "Novopay",
    "order_id": "",
    "description": "Load Wallet",
    "prefill": {
      "name": "",
      "email": "test@test.com",
      "contact": "",
      "method": ""
    },
    "modal": {},
    "theme": {
      "color": "#0096C5"
    }
  };
  public proceed() {
    this.RAZORPAY_OPTIONS.amount = 100 + '00';

    // binding this object to both success and dismiss handler
    this.RAZORPAY_OPTIONS['handler'] = this.razorPaySuccessHandler.bind(this);

    // this.showPopup();

    let razorpay = new Razorpay(this.RAZORPAY_OPTIONS)
    razorpay.open();
  }
  public razorPaySuccessHandler(response) {
    console.log(response,'hi');
    this.razorpayResponse = `Razorpay Response`;
    this.showModal = true;
    this.cd.detectChanges()
    document.getElementById('razorpay-response').style.display = 'block';
  }
}
