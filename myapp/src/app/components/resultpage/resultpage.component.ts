import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-resultpage',
  templateUrl: './resultpage.component.html',
  styleUrls: ['./resultpage.component.scss'],
})
export class ResultpageComponent implements OnInit {
data:any;
@Input() editable: boolean = false; // doesn't have to be an @Input
  constructor() {}

  ngOnInit(): void {
   this.data=JSON.parse(localStorage.getItem("data"))
  }

}
