import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ReviewModel } from 'src/app/models/review-model';
import { ReviewServiceService } from 'src/app/services/review-service.service';
import { AlertService } from 'src/app/shared/services/alert/alert.service';
import { DataService } from 'src/app/services/data.service';
import { AlertActionModel } from 'src/app/shared/models/alert-action-model';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { FileUploader } from 'ng2-file-upload';

const uploadAPI = 'http://localhost:3000/api/upload'; 
@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss'],
})
export class HomepageComponent implements OnInit {
  title = 'ng8fileuploadexample';
  public uploader: FileUploader = new FileUploader({ url: uploadAPI, itemAlias: 'file' });
  constructor(
    private reviewServiceService: ReviewServiceService,
    private alertService: AlertService,
    private dataService: DataService,
    private location: Location,
    private route: ActivatedRoute,
    private router:Router
  ) { }
  reviewModel: ReviewModel = {};

  email = new FormControl('', [Validators.required, Validators.email]);


  ngOnInit(): void { 
    this.uploader.onAfterAddingFile = (attachements) => { attachements.withCredentials = false; };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
         console.log('FileUpload:uploaded successfully:', item, status, response);
    };
  }
  getErrorMessage() {
    if (this.email.hasError('required')) {
      return 'You must enter a value';
    }

    return this.email.hasError('email') ? 'Not a valid email' : '';
  }
  /**
   * function will invoke when a save button click to call create() for post API request
   */
  saveForm() {
    this.reviewServiceService
      .createOrUpdate(this.reviewModel)
      .subscribe((res) => {
        if (res && res._id) {
          localStorage.setItem("data",JSON.stringify(this.reviewModel))
          this.alertService.success(
            `Review  ${this.reviewServiceService.create ? 'Added' : 'Added'
            } successfully`,
            'AddorUpdate'
          );
          const alertListener = this.alertService
            .getAction('AddorUpdate')
            .subscribe((alertActionModel: AlertActionModel) => {
              if (
                alertActionModel.actionId === 1 &&
                alertActionModel.functionName === 'AddorUpdate'
              ) {
                alertListener.unsubscribe();
                this.action();
              }
            });
        }
      });
  }
    /**
   *  function to locate to result page
   */
  public action() {
    this.router.navigateByUrl('/result');
  }
  
  
}
