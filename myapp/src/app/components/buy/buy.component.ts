import { Component, OnInit } from '@angular/core';
import { OrderModel } from 'src/app/models/order';
import {CheckoutModel }from 'src/app/models/checkout';
import { OrderService } from 'src/app/services/order.service';
import { AlertService } from 'src/app/shared/services/alert/alert.service';
import { AlertActionModel } from 'src/app/shared/models/alert-action-model';
import { ChangeDetectorRef } from '@angular/core';
import { ExternalLibraryService } from 'src/app/util';

declare let Razorpay: any; 

@Component({
  selector: 'app-buy',
  templateUrl: './buy.component.html',
  styleUrls: ['./buy.component.scss']
})
export class BuyComponent implements OnInit {

  constructor(
    private orderService: OrderService,
    private alertService: AlertService,
    private razorpayService: ExternalLibraryService,
    private cd: ChangeDetectorRef
  ) { }
name = 'Angular';
response;
razorpayResponse;
showModal = false;
  orderModel: OrderModel = {};
  checkoutModel:CheckoutModel={};
  ngOnInit(): void { this.razorpayService
    .lazyLoadLibrary('https://checkout.razorpay.com/v1/checkout.js')
    .subscribe();
  }
  RAZORPAY_OPTIONS = {
    "key": "rzp_test_7HpekRp5jhnTOO",
    "amount": "",
    "name": "Tapau",
    "order_id": "",
    "description": "Load Wallet",
    "prefill": {
    "name": "",
    "email": "test@test.com",
    "contact": "",
    "method": ""
    },
    "modal": {},
    "theme": {
    "color": "#0096C5"
    }
    };
  /**
   * function will invoke when a save button click to call create() for post API request
   */
  saveForm() {
    this.orderService
      .createOrUpdate(this.orderModel)
      .subscribe((res) => {
        this.RAZORPAY_OPTIONS.amount =res.order_total+ '';
        this.RAZORPAY_OPTIONS.order_id=res.order_id;
        console.log(this.RAZORPAY_OPTIONS.amount,'money')
        console.log(this.RAZORPAY_OPTIONS.order_id,'order id')
    // binding this object to both success and dismiss handler
    this.RAZORPAY_OPTIONS['handler'] = this.razorPaySuccessHandler.bind(this);
    // this.showPopup();
    let razorpay = new Razorpay(this.RAZORPAY_OPTIONS)
    razorpay.open();
        if (res && res._id) {
          localStorage.setItem("data",JSON.stringify(this.orderModel))
          this.alertService.success(
            `Review  ${this.orderService.create ? 'Added' : 'Added'
            } successfully`,
            'AddorUpdate'
          );
          
          const alertListener = this.alertService
            .getAction('AddorUpdate')
            .subscribe((alertActionModel: AlertActionModel) => {
              if (
                alertActionModel.actionId === 1 &&
                alertActionModel.functionName === 'AddorUpdate'
              ) {
                alertListener.unsubscribe();

              }
            });
        }
      });
  }
   
  public razorPaySuccessHandler(response) {
    console.log(response,'hi monuse');
    this.orderService.createCheckout(this.checkoutModel=response)
    console.log(this,this.checkoutModel,'this is')
    this.razorpayResponse = `Razorpay Response`;
    this.showModal = true;
    this.cd.detectChanges()
    document.getElementById('razorpay-response').style.display = 'block';
    }
  
}
