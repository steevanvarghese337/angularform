import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { SharedModule } from './shared/shared.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { HomepageComponent } from './components/homepage/homepage.component';
import { ResultpageComponent } from './components/resultpage/resultpage.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { HttpErrorHandler } from './network/http-error-handler.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FileUploadModule } from 'ng2-file-upload';
import { BuyComponent } from './components/buy/buy.component';
import { PayComponent } from './components/pay/pay.component';


@NgModule({
  declarations: [AppComponent, HomepageComponent, ResultpageComponent, BuyComponent, PayComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    GooglePlaceModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    FileUploadModule
  ],
  providers: [HttpErrorHandler],
  bootstrap: [AppComponent],
})
export class AppModule {}
