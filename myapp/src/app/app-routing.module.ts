import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomepageComponent } from './components/homepage/homepage.component';
import { ResultpageComponent } from './components/resultpage/resultpage.component';
import {BuyComponent} from './components/buy/buy.component'
import { PayComponent } from './components/pay/pay.component';
const routes: Routes = [
  {
    path: '',
    component: HomepageComponent,
  },
  {
    path: 'result',
    component: ResultpageComponent,
  },
  {
    path:'buy',
    component:BuyComponent
  },
  {
    path:'pay',
    component:PayComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
