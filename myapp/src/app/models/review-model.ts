export interface ReviewModel {
  _id?: string;
  name?: string;
  mobile?: number;
  email?: string;
  anonymous?: string;
  pictma?: string;
  tags?: string;
  location?: string;
  attachements?:any;
  content?: string;
}
