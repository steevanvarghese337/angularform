export interface OrderModel {
  _id?: string;
  amount?: number;
  order_total?: number;
  order_id?: string;
}
